# Установка Visual Studio Code (Windows)

`Visual Studio Code`, также обычно называемый `VS Code`, представляет собой редактор исходного кода, созданный Microsoft для Windows, Linux и macOS. Установщик можно скачать с [сайта](https://code.visualstudio.com/).

Шаг 1:

![Установка пакетов VS Code - шаг 1](images/vs-code-install-step1.png)

Шаг 2:

![Установка пакетов VS Code - шаг 2](images/vs-code-install-step2.png)

Шаг 3:

![Установка пакетов VS Code - шаг 3](images/vs-code-install-step3.png)

Шаг 4:

![Установка пакетов VS Code - шаг 4](images/vs-code-install-step4.png)

Шаг 5:

![Установка пакетов VS Code - шаг 5](images/vs-code-install-step5.png)

Шаг 6:

![Установка пакетов VS Code - шаг 6](images/vs-code-install-step6.png)

Шаг 7:

![Установка пакетов VS Code - шаг 7](images/vs-code-install-step7.png)

Шаг 8 (ожидание установки):

![Установка пакетов VS Code - шаг 8](images/vs-code-install-step8.png)

Шаг 9 (завершение установки):

![Установка пакетов VS Code - шаг 9](images/vs-code-install-step9.png)
