# Создание проекта и его отладка в Visual Studio Code (Windows)

В окне Visual Studio Code необходимо выбрать или создать папку, в которой разместится проект программы (папка должна быть пустой):

![Debug в VS Code - шаг 1](images/vs-code-debug-step1.png)

![Debug в VS Code - шаг 2](images/vs-code-debug-step2.png)

![Debug в VS Code - шаг 3](images/vs-code-debug-step3.png)

Далее вызвать палитру команд:

![Debug в VS Code - шаг 4](images/vs-code-debug-step4.png)

В поле ввода палитры ввести `CMake: Quick Start` и нажать клавишу `Enter`:

![Debug в VS Code - шаг 5](images/vs-code-debug-step5.png)

Если вы не добавили в переменные окружения путь папки, содержащий набор инструментов для разработки, то следующая форма будет такой:

![Debug в VS Code - шаг 6](images/vs-code-debug-step6.png)

Если вы добавили в переменные окружения необходимое значение, то необходимо перезапустить IDE.

Далее выбрать набор инструментов:

![Debug в VS Code - шаг 7](images/vs-code-debug-step7.png)

Ввести название проекта:

![Debug в VS Code - шаг 8](images/vs-code-debug-step8.png)

Указать, что проект является исполняемой программой (`Executable`):

![Debug в VS Code - шаг 9](images/vs-code-debug-step9.png)

Убедиться, что проект создан:

![Debug в VS Code - шаг 10](images/vs-code-debug-step10.png)

Для удобства можно включить автосохранение при изменении файлов:

![Debug в VS Code - шаг 11](images/vs-code-debug-step11.png)

Добавьте кириллические букв в вывод программы, чтобы убедиться, что набор инструментов настроен правильно.

```cpp
#include <iostream>

int main(int, char**) {
    std::cout << "Вы продаете рыбов? Нет просто показываю...";
    std::cout << std::endl;
    std::cout.flush();
    return 0;
}
```

Запустите открытого файла:

![Debug в VS Code - шаг 12](images/vs-code-debug-step12.png)

Укажите инструмент для сборки и отладки:

![Debug в VS Code - шаг 13](images/vs-code-debug-step13.png)

В случае успеха в панели консоли будет виден вывод:

![Debug в VS Code - шаг 14](images/vs-code-debug-step14.png)
