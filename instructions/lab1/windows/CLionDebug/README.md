# Создание проекта и его отладка в CLion (Windows)

В приветственном окне CLion выберете создание проекта:

![Debug в VS Code - шаг 1](images/CLion-debug-step1.png)

Укажите папку проекта (папа проекта должна быть пустой) и используемый стандарт языка:

![Debug в VS Code - шаг 2](images/CLion-debug-step2.png)

После создания появляется окно инструментов разработки. При правильной установки Cygwin, CLion самостоятельно установит необходимые параметры. Cygwin необходимо выбрать по умолчанию. Для этого выберите в списке набор инструментов и нажмите кнопку `▲`:

![Debug в VS Code - шаг 3](images/CLion-debug-step3.png)

Подробно о Toolchains по [ссылке](https://www.jetbrains.com/help/clion/how-to-create-toolchain-in-clion.html).

Дождитесь, когда CLion выполнит все фоновые задачи. 

Измените файл `main.cpp` следующим образом:

```cpp
#include <iostream>

int main(int, char**) {
    std::cout << "Вы продаете рыбов? Нет просто показываю...";
    std::cout << std::endl;
    std::cout.flush();
    return 0;
}
```

Запустите проект на отладку, не забыв поставить точку останова на `return 0`:

![Debug в VS Code - шаг 4](images/CLion-debug-step4.png)

Полная документация о CLion по ссылке [здесь](https://www.jetbrains.com/help/clion/clion-quick-start-guide.html).
