# Лабораторная работа №1

Название: `Интегрированные среды разработки Visual Studio Code и CLion`.

Алиас: `hello-world`.

## Задание

Данная лабораторная работа является вводной. В ней предстоит сделать:

- Изучить материал по установке и настройке средств разработки.
- В зависимости от предпочтений и возможностей, установить Visual Studio Code или CLion. Для CLion необходимо получить электронный адрес `@student.bmstu.ru`. Если пока нет электронного адреса, то можно воспользоваться "Free 30-day Trial".
- Согласно указаниям преподавателя, получить репозиторий в GitLab для выполнения лабораторных работ.
- Выполнить задание в GitLab:
  - Создать новую ветвь от ветви по умолчанию в полученном репозитории.
  - Склонировать репозиторий локально и перейти в созданную ветвь.
  - Раскомментировать код `lab1\main.cpp`.
  - Собрать и запустить программу.
  - Убедиться в корректной работе программы.
  - Зафиксировать локальные изменения и отправить их в удаленный репозиторий (GitLab)
  - Создать запрос на слияние созданной ветви в ветвь по умолчанию.
  - Убедиться в прохождении теста.
  - Сообщить о выполнении преподавателю.

На момент подготовки данного текста пропущен раздел с описанием работы в Git (будет добавлен значительно позже). О том, как работать с Git, необходимо самостоятельно изучить ресурсы типа:

- [Введение в Git: от установки до основных команд](https://tproger.ru/translations/beginner-git-cheatsheet/)
- [Git за полчаса: руководство для начинающих](https://proglib.io/p/git-for-half-an-hour)
- [Что такое GIT простым языком? Как работает, основные команды GIT](https://www.youtube.com/watch?v=buygCuSqBsA)
- ... и другое по запросу в YouTube и в Яндекс.

В качестве Git GUI рекомендуется использовать [GitKraken](https://www.gitkraken.com/).

## Разработка в операционных систем в Windows 10 и Windows 11

При работе в операционной системе Windows 10 и Windows 11 на выбор предлагается использовать Visual Studio Code или CLion для разработки программ на C++.

Разработчик программ C++ всегда в своем наборе инструментов имеет компилятор и отладчик. В качестве компилятора на Windows можно использовать [GCC](https://gcc.gnu.org/), а для отладки - [GDB](https://www.sourceware.org/gdb/). Получить их можно из наборов инструментов и библиотек такие, как MinGW (простой способ установить, через [MSYS2](https://www.msys2.org/)) и [Cygwin](https://cygwin.com/).

Информацию по работе с Visual Studio Code можно получить из [документации](https://code.visualstudio.com/docs).

Для начала разработки программ на С++ в Visual Studio Code имеется [серия статей из документации](https://code.visualstudio.com/docs/cpp/). Для Windows предлагается набор инструментов разработчика получить из [MinGW](https://code.visualstudio.com/docs/cpp/config-mingw) или развернуть их в [WSL](https://code.visualstudio.com/docs/cpp/config-wsl).

В данной работе для работы в Visual Studio Code предлагается:

1. Установить MSYS2 (смотреть [здесь](windows/MSYS2/README.md)).
2. Установить CMake (смотреть [здесь](windows/CMake/README.md)).
3. Установить Visual Studio Code (смотреть [здесь](windows/VSCodeInstall/README.md)).
4. Установить расширения для Visual Studio Code (смотреть [здесь](VSCodeExtensions/README.md)).
5. Создать тестовый проект и выполнить его отладку (смотреть [здесь](windows/VSCodeDebug/README.md)).

Об отладке в Visual Studio Code подробно рассказано в [статье](https://code.visualstudio.com/docs/editor/debugging).

Информацию по работе с CLion можно получить из [документации](https://www.jetbrains.com/help/clion/).

В данной работе для работы CLion предлагается:

1. Установить Cygwin (смотреть [здесь](windows/Cygwin/README.md)).
2. Установить CMake (смотреть [здесь](windows/CMake/README.md)).
3. Установить CLion (смотреть [здесь](windows/CLionInstall/README.md)).
4. Создать тестовый проект и выполнить его отладку (смотреть [здесь](windows/CLionDebug/README.md)).

Об отладке в CLion подробно рассказано в [статье](https://www.jetbrains.com/help/clion/configuring-debugger-options.html).

### Решение возможных проблем

Если имеется проблема с локализацией при работе с Visual Studio Code, то рекомендуется попробовать CLion. Перед этим необходимо удалить MSYS2 и добавленную переменную окружения. Также необходимо убедиться, что папка с установкой MSYS2.

Если все еще имеются проблемы, то рекомендуется обратить внимание на [WSL](https://code.visualstudio.com/docs/cpp/config-wsl).

## Разработка в семействе операционных систем в Linux

Для работы в Visual Studio Code в Linux рекомендуется прочитать данную [статью](https://code.visualstudio.com/docs/cpp/config-linux).

Установка CLion представлена в [статье](https://www.jetbrains.com/help/clion/installation-guide.html).

## Разработка в операционной системе macOS

Для работы в Visual Studio Code в Linux рекомендуется прочитать данную [статью](https://code.visualstudio.com/docs/cpp/config-clang-mac).

Установка CLion представлена в [статье](https://www.jetbrains.com/help/clion/installation-guide.html).
